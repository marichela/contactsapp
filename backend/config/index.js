const config = exports;

config.tokenSecret = {
  JWTSECRETWORD: process.env.JWTSECRETWORD,
  EXPIRE: 2592000 // expires in one month
};

config.clientHomePageUrl = process.env.FRONTEND_URL;
