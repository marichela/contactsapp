require('dotenv').config();
const express = require('express');

const server = express();
const bodyParser = require('body-parser');
const domainMiddleware = require('express-domain-middleware');
const http = require('http');
const path = require('path');

// CONFIG
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(domainMiddleware);

// allow authentication headers
server.all('*', (req, res, next) => {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Credentials', true);
  res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
  res.set(
    'Access-Control-Allow-Headers',
    'X-Requested-With, Content-Type, Authorization, X-Custom-Header'
  );

  if (req.method === 'OPTIONS') return res.sendStatus(200);

  return next();
});
//= ============================================================
const frontendFolder = `${process.env.NODE_ENV === 'development' ? '..' : '.'}/frontend/build`;
server.use(express.static(path.join(__dirname, frontendFolder)));

//= ============================================================
// ROUTES
server.use(require('./js/app'));

if (process.env.NODE_ENV !== 'test') {
  http.createServer(server).listen(4000, function Server() {
    console.log(`My http server listening on port ${4000}...`);
  });
}
module.exports = server;
