const expressValidator = require('express-validator');

const validator = exports;

validator.checkPostProfile = [
  expressValidator
    .body('username')
    .not()
    .isEmpty()
    .withMessage('Username must have characteres'),
  expressValidator
    .body('password')
    .not()
    .isEmpty()
    .withMessage('Password must have characteres')
];
