/* eslint-disable consistent-return */
const express = require('express');

const router = express();

const JWT = require('jsonwebtoken');
const expressValidator = require('express-validator');
const validator = require('./validators/validators');
const util = require('./utils/util');
const config = require('../config/index');

const CLIENT_HOME_PAGE_URL = config.clientHomePageUrl;
/* ====================================================================================================
// POST LOGIN
==================================================================================================== */
router.post('/login', validator.checkPostProfile, async (req, res) => {
  const errors = expressValidator.validationResult(req);
  if (!errors.isEmpty()) return res.status(422).json(errors.array());
  try {
    const { password } = req.body;
    const { username } = req.body;
    const users = util.readFile();
    users.forEach(element => {
      if (element.username === username && element.password === password) {
        const token = JWT.sign({ username: element.username }, config.tokenSecret.JWTSECRETWORD, {
          expiresIn: config.tokenSecret.EXPIRE
        });
        return res.redirect(`${CLIENT_HOME_PAGE_URL}/callback/?token=${token}`);
      }
      return res.status(403).send({ code: 'ERR_FORBIDDEN', MSG: 'Forbidden action' });
    });
  } catch (err) {
    return res.status(500).send({ code: 'ERR_SERVER_ERROR', msg: err });
  }
});

/* ====================================================================================================
// GET CONTACTS
==================================================================================================== */
const AUTH_MIDDLEWARE = [util.jwtMW, util.hydrateUser];

router.get('/contacts', AUTH_MIDDLEWARE, async (req, res) => {
  try {
    const contacts = await util.goToUrlContactsJson(process.env.URL_CONTACTS);
    return res.status(200).send(contacts);
  } catch (err) {
    return res.status(500).send({ code: 'ERR_SERVER_ERROR', msg: err });
  }
});

module.exports = router;
