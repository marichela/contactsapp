/* eslint-disable consistent-return */
const fs = require('fs');
const axios = require('axios');
const exjwt = require('express-jwt');
const handleError = require('./handleError');
const config = require('../../config/index');

const util = exports;

util.readFile = () => {
  try {
    const usersFIle = fs.readFileSync('js/utils/usersDB.json', 'utf8'); // without debug
    // const usersFIle = fs.readFileSync(`${process.env.HOME}js/utils/usersDB.json`, 'utf8');
    return JSON.parse(usersFIle);
  } catch (error) {
    handleError.logErro = error;
    throw error;
  }
};

util.jwtMW = exjwt({
  secret: config.tokenSecret.JWTSECRETWORD
});

util.hydrateUser = async (req, res, next) => {
  const reqUser = req.user.username;
  const users = util.readFile();
  users.forEach(element => {
    if (element.username === reqUser) {
      return next();
    }
  });
};

util.goToUrlContactsJson = async url => {
  try {
    const res = await axios.get(url);
    await new Promise(resolve => setTimeout(resolve, 1500));
    return res.data;
  } catch (err) {
    handleError.logError(err);
    throw err;
  }
};
