module.exports = {
    "env": {
        "commonjs": true,
        "es6": true,
        "node": true
    },
    "extends": ["airbnb", "prettier"],
    "plugins": ["prettier"],
    "rules": {
      "prettier/prettier": ["error"]
    }
};