/* eslint-disable consistent-return */
/* eslint-disable no-undef */
require('dotenv').config();
const request = require('supertest');
const app = require('../server');

let token;

beforeAll(done => {
  request(app)
    .post('/login')
    .send({ username: 'marichela', password: '12345678' })
    .end((err, response) => {
      token = response.body.token; // save the token!
      done();
    });
});
/* ====================================================================================================
// TEST FOR POST LOGIN
==================================================================================================== */
describe('GET /login', () => {
  test('Login without username and password', done => {
    request(app)
      .post('/login')
      .send({})
      .expect(422)
      .end(err => {
        if (err) return done(err);
        done();
      });
  });

  test('Login with wrong username', async done => {
    await request(app)
      .post('/login')
      .send({ username: 'HOLA', password: '12345678' })
      .expect(403);
    done();
  });
});

/* ====================================================================================================
// TEST FOR GET CONTACTS
==================================================================================================== */
describe('GET /contacts', () => {
  test('It should require authorization for contacts', () => {
    return request(app)
      .get('/contacts')
      .then(response => {
        expect(response.statusCode).toBe(401);
      });
  });

  //= ==================================================================================================
  // send the token - should respond with a 200
  test('It responds with JSON', done => {
    return request(app)
      .get('/contacts')
      .set('Authorization', `Bearer ${token}`)
      .then(response => {
        expect(response.statusCode).toBe(200);
        expect(response.type).toBe('application/json');
        done();
      });
  });
});
