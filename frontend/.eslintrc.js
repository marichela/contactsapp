module.exports = {
    env: {
      browser: true,
      es6: true,
    },
    extends: [
      'plugin:react/recommended',
      'plugin:fp/recommended',
      'plugin:ramda/recommended',
    ],
    globals: {
      Atomics: 'readonly',
      SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
      ecmaFeatures: {
        jsx: true,
      },
      ecmaVersion: 2018,
      sourceType: 'module',
    },
    plugins: ['react', 'fp', 'react-hooks', 'ramda'],
    rules: {
      'import/prefer-default-export': 'warn',
      'implicit-arrow-linebreak': 'off',
      'object-curly-spacing': ['warn', 'always'],
      'no-underscore-dangle': ['error', { allow: ['__', '_id'] }],
      // React config
      'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
      'react/display-name': 'off',
      'react/no-unused-prop-types': 'warn',
      'react/sort-prop-types': 'off',
      'react/forbid-prop-types': [
        2,
        {
          forbid: ['any', 'array'],
          checkContextTypes: false,
          checkChildContextTypes: false,
        },
      ],
      'react/prop-types': 'off',
      'react/no-array-index-key': 'warn',
      // React hooks config
      'react-hooks/rules-of-hooks': 'error',
      'react-hooks/exhaustive-deps': 'off',
      // FP Plugin config config
      'fp/no-class': 'warn',
      'fp/no-loops': 'error',
      'fp/no-mutating-methods': 'warn',
      'fp/no-mutation': 'warn',
      'fp/no-mutating-methods': [
        'warn',
        {
          allowedObjects: ['_', 'R', 'fp', 'Actions'],
        },
      ],
      'fp/no-nil': 'off',
      'fp/no-rest-parameters': 'off',
      'fp/no-unused-expression': 'off',
      'fp/no-mutation': [
        'warn',
        { exceptions: [{ property: 'propTypes' }, { property: 'defaultProps' }] },
      ],
    },
  };