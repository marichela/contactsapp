import { API } from "./commonApi"
import { endpoints } from "./endpoints"

const contactsApi = {
    login: (body) => (
        API.post(
            endpoints.AUTH, 
            body
        )
    ),
    loadContacts: () => {
        API.get(
            endpoints.CONTACTS
        )
    }
}

export default contactsApi