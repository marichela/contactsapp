import axios from "axios"
import { BASE_URL } from "../config"
import { TOKEN_KEY } from "../constants/localStorageKeys"
const searchLocalToken = () => {
    const token = localStorage.getItem(TOKEN_KEY)
    if (token) {
        return { Authorization: "Bearer "+ token}
    }
    return {}
}

export const API = axios.create({
    baseURL: BASE_URL,
    headers: searchLocalToken()
})