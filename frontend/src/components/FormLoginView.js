import React from "react"
import { Button } from "react-bootstrap"

const FormLoginView = ({
    handleChange, 
    handleSubmit,
    inputs
}) => {
    return(
        <div>
            <input
            type="text"
            name="username"
            value={inputs.username}
            onChange={handleChange}
            placeholder="username"
            required
            />
            <input
            type="password"
            name="password"
            value={inputs.password}
            onChange={handleChange}
            placeholder="password"
            required
            />
            <Button onClick={handleSubmit}>Login</Button>
            
        </div>
    )
}
export default FormLoginView
