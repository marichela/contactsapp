import React, { useState } from "react"
import contactsApi from "../../api/contactsApp"
import FormLoginView from "../FormLoginView"
import { Container, Row, Col} from "react-bootstrap"
const Landing = () => {
    const [inputs, setInputs] = useState({})

    const handleChange = (event) => {
        const value = event.target.value
        const name = event.target.name
        console.log("hola",value, name)
        setInputs(inputs => ({...inputs, [name]: value}))
    }

    const handleSubmit = async (event) => {
        const dataAPI = inputs
        event.preventDefault()
        try{
            await contactsApi.login(dataAPI)
        } catch (error) {
            alert("Please check whether all fields are filled right")
        }
    }
    return(
        <Row>
            <Col sm={12} md={8}>
                <img 
                    src="https://wallpapersite.com/images/pages/pic_w/18588.jpg"
                />
            </Col>
            <Col sm={12} md={4}>
                <FormLoginView
                handleChange={handleChange}
                handleSubmit={handleSubmit}
                inputs={inputs}
                />
            </Col>
        </Row>

    )
}

export default Landing