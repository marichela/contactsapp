import { BrowserRouter, Route, Switch } from "react-router-dom"
import React from "react"
import routes from "../../constants/routes"
import Landing from "../landing/Landing"
const App = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route 
                    exact path={routes.LANDING}
                    component={Landing}/>
            </Switch>
        </BrowserRouter>
    )
}

export default App